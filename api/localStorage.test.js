import { buildLocalStorage, buildKeyValueModelServices, buildKeyValueStore } from './localStorage'
import { range, tuple1, tuple2 } from '../utility'

const expectFn = expect.any(Function)

const buildMockStorage = () => {
    const store = {}

    const mockGet = jest.fn( key => store[key])
    const mockSet = jest.fn( (key, value) => store[key] = value )
    const mockGetAllKeys = jest.fn( () => Object.keys(store))

    const storage = {
        getItem: async (...args) => mockGet(...args),
        setItem: async (...args) => mockSet(...args),
        getAllKeys: async () => mockGetAllKeys()
    }

    return {
        storage,
        mockGet,
        mockSet,
        mockGetAllKeys
    }
}

describe('buildKeyValueStore', () => {
    const { storage, mockGet, mockSet } = buildMockStorage()

    const testStore = buildKeyValueStore(storage)

    it('should build a key value store', () => {
        expect(testStore).toBeDefined()
        expect(testStore).toMatchObject({
            get: expectFn,
            set: expectFn,
            increase: expectFn
        })
    })

    it('should attempt to set items', async () => {
        const myItem = {a: 'value'}
        await testStore.set('myKey', myItem)

        expect(mockSet).toHaveBeenCalledWith('@hazlo:myKey', myItem)
    })

    it('should attempt to get items', async () => {
        await testStore.get('myKey')

        expect(mockGet).toHaveBeenCalledWith('@hazlo:myKey')
    })

    it('should get items, returning a default on fail if provided', async () => {
        const value = await testStore.get('aKey', 'fallback')

        expect(value).toEqual('fallback')
    })

    it('should be able to increase a value', async () => {
        const KEY = 'increaseTest'
        await testStore.set(KEY, 0)
        await testStore.increase(KEY, 1)

        const val1 = await testStore.get(KEY)

        expect(val1).toEqual(1)

        await testStore.increase(KEY, 2)
        const val2 = await testStore.get(KEY)

        expect(val2).toEqual(2)
    })

    it('should make no changes if attempting to increase to a lower value', async () => {
        const KEY = 'increaseCheckTest'

        await testStore.set(KEY, 5)
        await testStore.increase(KEY, 6)
        const val1 = await testStore.get(KEY)
        expect(val1).toEqual(6)

        await testStore.increase(KEY, 4)
        const val2 = await testStore.get(KEY)
        expect(val2).toEqual(6)
    })

    describe('getStartsWith', () => {
        it('should provide all the values of keys that start with a given string', async () => {
            const collectionRoot = 'testRoot'

            await Promise.all([
                testStore.set(`${collectionRoot}:1`, 'a'),
                testStore.set(`${collectionRoot}:2`, 'b'),
                testStore.set(`${collectionRoot}:3`, 'c'),
                testStore.set(`${collectionRoot}:4`, 'd'),
            ])

            const allValues = await testStore.getStartsWith(collectionRoot)

            expect(allValues).toEqual(expect.objectContaining({
                1: 'a',
                2: 'b',
                3: 'c',
                4: 'd',
            }))
        })
    })
})

describe('buildKeyValueModelServices', () => {
    const TestModels = {
        TEST: 'test',
        TEST_A: 'test_a'
    }

    const testStore = buildKeyValueStore(buildMockStorage().storage)
    const testServices = buildKeyValueModelServices(testStore, [TestModels.TEST, TestModels.TEST_A])

    describe('key service', () => {
        it('should provide the next free key', async () => {
            expect(testServices).toHaveProperty(TestModels.TEST)
            const testService = testServices[TestModels.TEST]

            expect(testService).toMatchObject({
                key: {
                    nextInstanceKey: expectFn,
                    canonical: expectFn
                },
                page: {
                    next: expectFn,
                    previous: expectFn,
                    current: expectFn
                },
            })

            const key = await testService.key.nextInstanceKey(TestModels.TEST)

            expect(key).toEqual(0)

            const key1 = await testService.key.nextInstanceKey(TestModels.TEST)
            expect(key1).toEqual(1)
        })

        it('should not provide duplicate keys', async () => {
            const testService = testServices[TestModels.TEST_A]

            const expectedKeys = range(100)

            const providedKeys = await Promise.all(
                expectedKeys.map( () => testService.key.nextInstanceKey(TestModels.TEST_A) )
            )

            expect(providedKeys).toEqual(expect.arrayContaining(expectedKeys))
        })
    })
})

describe('buildLocalStorage', () => {
    const TestModels = {
        TEST: 'test',
        TEST_A: 'test_a',
    }

    const { storage, mockSet } = buildMockStorage()
    const testStorage = buildLocalStorage(storage, [TestModels.TEST, TestModels.TEST_A])

    it('should build a storage controller', () => {
        expect(testStorage).toBeDefined()
        expect(testStorage).toMatchObject({
            get: {
                one: expectFn,
                all: expectFn,
                page: expectFn,
                pageNext: expectFn,
            },
            set: {
                new: expectFn,
                update: expectFn
            },
        })
    })

    describe('localStorage', () => {
        it('should allow adding a new item to a model', async () => {
            const resKeyA = await testStorage.set.new(TestModels.TEST, 'a')

            expect(mockSet).toHaveBeenCalledWith('@hazlo:test:0', 'a')
            expect(resKeyA).toEqual(0)

            const setA = await testStorage.get.one(TestModels.TEST, resKeyA)
            expect(setA).toEqual('a')

            const resKeyB = await testStorage.set.new(TestModels.TEST, 'b')
            expect(mockSet).toHaveBeenCalledWith('@hazlo:test:1', 'b')
            expect(resKeyB).toEqual(1)


            const setB = await testStorage.get.one(TestModels.TEST, resKeyB)
            expect(setB).toEqual('b')
        })

        it('should allow updating an item', async () => {
            const resKey = await testStorage.set.new(TestModels.TEST, 'original')

            const originalVal = await testStorage.get.one(TestModels.TEST, resKey)

            expect(originalVal).toEqual('original')

            await testStorage.set.update(TestModels.TEST, resKey, 'updated')
            const updatedVal = await testStorage.get.one(TestModels.TEST, resKey)

            expect(updatedVal).toEqual('updated')
        })

        it('should be able to set many items of a model at once, and get them', async () => {
            const keyValuesToStore = range(15).map( k => [k, 'val ' + k])
            
            await Promise.all(
                keyValuesToStore
                .map( async x => await testStorage.set.new(TestModels.TEST_A, tuple2(x)))
            )

            const storedValues = await testStorage.get.all(TestModels.TEST_A)
            expect(storedValues).toEqual(expect.objectContaining(
                keyValuesToStore.reduce(
                    (a, c) => Object.assign(a, {[tuple1(c)]: expect.stringMatching(tuple2(c))}),
                    {}
                )
            ))
        })
    })
})
/* TODO
 * test starting storage with an existing nextInstanceKey
 * paging
 */

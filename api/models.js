const MODEL = {
    TASK: 'task',
    GROUP: 'group',
    USER: 'user',
    CHAT: 'chat',
}

export default MODEL

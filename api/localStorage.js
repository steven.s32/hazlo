import track from '../utility/track'
import { tuple1, tuple2, keyValueTupleToObject } from '../utility'
import { isVoid } from '../utility/typeGuards'
import Model from './models'

const ROOT = '@hazlo'
const isGreaterThan = (a, b) => a > b

export const buildKeyValueStore = storage => {
    const name = k => `${ROOT}:${k}`
    const unname = root => topScopeKey => {
        const matched = topScopeKey.match(RegExp(`(${root}:)(.+)`))

        if (matched) {
            return matched[2]
        }
    }

    const get = async (key, _default) => {
        try {
            const value = await storage.getItem(name(key))

            if (!isVoid(value)) {
                return value
            }
        } catch (e) {
            track.error('Error retrieving key', key, e)
        }

        return _default
    }

    const set = async (key, item) => {
        try {
            await storage.setItem(
                name(key),
                item
            )

            return item
        } catch (e) {
            track.error('Error setting item', e, key, item)
        }
    }

    const conditionalMutate = condition => async (key, value) => {
        const existingValue = await get(key)

        if (isVoid(existingValue) || condition(value, existingValue)) {
            return await set(key, value)
        }
    }

    return {
        get,
        set,
        increase: conditionalMutate(isGreaterThan),
        getStartsWith: async keyStart => {
            const allTopScopeKeys = await storage.getAllKeys()

            const asyncResults = await Promise.all(
                allTopScopeKeys
                .map( unname(ROOT) )
                .filter( Boolean )
                .filter( k => k.startsWith(keyStart) )
                .map( async k => [k, await get(k)] )
            )

            const results = asyncResults
                .map( t => [unname(keyStart)(tuple1(t)), tuple2(t)] )
                .reduce(keyValueTupleToObject, {})

            return results
        }
    }
}

const defaultModels = [
    Model.TASK,
    Model.GROUP,
    Model.USER,
    Model.CHAT,
]

export const buildKeyValueModelServices = (keyValueStore, models=[]) => {
    const pageBuilder = () => {
        let currentPage = 0 // TODO this should return a left boundary element index rather than page

        return {
            next: () => ++currentPage,
            previous: () => --currentPage,
            current: () => currentPage,
        }
    }

    const keyBuilder = model => {
        const canonical = (model, itemKey) => `${model}:${itemKey}`

        let nextFreeKey = null

        return {
            canonical,
            nextInstanceKey: async () => {
                const storedNextKey__Key = `_meta:nextKey:${model}`

                if (nextFreeKey === null) {
                    const storedNextKey = await keyValueStore.get(storedNextKey__Key, 0)

                    if (nextFreeKey === null) { // still null after await
                        nextFreeKey = storedNextKey
                    }
                }

                keyValueStore.increase(storedNextKey__Key, nextFreeKey + 1)

                return nextFreeKey++
            },
            root: model
        }
    }

    const services = models.map(
        model => ([model, {
            page: pageBuilder(),
            key: keyBuilder(model)
        }]))
        .reduce(keyValueTupleToObject, {})

    return services
}

export const buildKeyValueLocalStorage = (keyValueStore, models=Model) => {
    const services = buildKeyValueModelServices(keyValueStore, models)

    const serviceFor = model => {
        const service = services[model]

        if (isVoid(service)) {
            throw Error(`Requested service for unknown model: ${model}`)
        }

        return service
    }

    return {
        get: {
            one: async (model, key) => {
                const service = serviceFor(model)
                const canonKey = service.key.canonical(model, key)

                return await keyValueStore.get(canonKey)
            },
            all: async model => {
                const modelPrefix = serviceFor(model).key.root

                return await keyValueStore.getStartsWith(modelPrefix)
            },
            page: async (model, page=1) => {},
            pageNext: async model => {},
        },
        set: {
            new: async (model, item) => {
                const service = serviceFor(model)

                const instanceKey = await service.key.nextInstanceKey()
                const key = service.key.canonical(model, instanceKey)

                const existingValue = await keyValueStore.get(key)

                if (isVoid(existingValue)) {
                    await keyValueStore.set(key, item)

                    return instanceKey
                } else {
                    track.error("New item's key was already in use", key, item)
                }
            },
            update: async (model, key, item) => {
                const service = serviceFor(model)

                return keyValueStore.set(service.key.canonical(model, key), item)
            },
        },
    }
}

export const buildLocalStorage = (storage, models=defaultModels) => buildKeyValueLocalStorage(buildKeyValueStore(storage), models)

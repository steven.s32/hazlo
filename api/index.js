import { local } from './persist'

const TASK = 'task'

const noop = () => {}

// Authentication
const authenticateUser = noop

// Tasks
const getTasks = noop
const addTask = async task => {
    const key = await persist.nextKey({model: TASK })
    return await persist.newItem({
        key,
        item: task
    })
}
const updateTask = noop

// Groups
const createGroup = noop
const getGroups = noop

export default {
    user: {
        authenticate: authenticateUser
    },

    task: {
        add: addTask,
        update: updateTask,
    },

    tasks: {
        get: getTasks,
    },

    group: {
        create: createGroup,
    },

    groups: {
        get: getGroups
    },
}

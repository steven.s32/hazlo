import AsyncStorage from './reactNativeAsyncStorage'

import { buildLocalStorage } from './localStorage'
import Model from './models'

export const models = Model
export const local = buildLocalStorage(AsyncStorage)

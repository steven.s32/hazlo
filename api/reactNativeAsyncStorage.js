import { AsyncStorage } from 'react-native'

export default { // FIXME may need to override differently
    ...AsyncStorage,
    getItem: async key => {
        const itemString = await AsyncStorage.getItem(key)
        return JSON.parse(itemString)
    },
    setItem: async (key, item) => {
        const itemString = JSON.stringify(item)

        return await AsyncStorage.setItem(key, itemString)
    },
}

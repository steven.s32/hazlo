/* eslint-disable no-console */

const log = {
    error: console.error,
    info: console.info,
    warn: console.warn,
}

export default log

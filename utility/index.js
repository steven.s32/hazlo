const randomWords = [
    'average',
    'downright',
    'trodden',
    'fulfill',
    'aluminum',
    'dairy',
    'pants',
    'cargo',
    'vertigo',
    'uncontrolled',
    'rhythm',
    'found',
    'ostrich',
    'caricature',
    'osmosis',
    'definite',
    'the',
    'the',
    'the',
    'the',
    'the',
    'a',
    'a',
    'a',
    'a',
    'a',
    'those',
    'i',
    'gravitate',
    'largely',
    'have',
    'have',
    'have',
    'have',
    'have',
    'have',
]

const randomDictionary = {
    words: randomWords,
    names: [
        'Sally',
        'Joe',
        'Steven',
        'Ghysel',
        'Maria',
        'Sun',
        'Rayme',
        'Guo',
        'Cheng',
        'Coa',
    ]
}

const minOne = x => Math.max(x, 1)
export const range = (num, arr=[]) => num === 0 ? [] : num <= 1 ? [num-1].concat(arr) : range(num - 1, [num-1].concat(arr))
export const maxRandom = num => Math.floor(Math.random() * num)
export const addRandomNumRandomWords = (max, dictionary='words') => {
    const wordList = randomDictionary[dictionary]

    return range(minOne(maxRandom(max))).map( () => wordList[maxRandom(wordList.length) ] ).join(' ')
}

const accessor = k => o => o[k]

export const tuple1 = accessor(0)
export const tuple2 = accessor(1)

export const objectMap = fn => obj => Object.keys(obj)
    .map( key => [key, fn(obj[key]), key] )
    .reduce(keyValueTupleToObject, {})

export const runOnce = fn => {
    let ran = false
    let results

    return (...args) => {
        if (ran) {
            return results
        }

        ran = true
        results = fn(...args)

        return results
    }
}

export const values = mapFn => obj => Object.keys(obj).map( key => mapFn(obj[key], key) )

export const extend = (obj, change) => Object.assign({}, obj, change)

export const objectToArray = (object, idString) => values( (el, key) => idString ? extend(el, {[idString]: key}) : el )(object)
export const keyValueTupleToObject = (a, c) => Object.assign(a, {[tuple1(c)]: tuple2(c)})

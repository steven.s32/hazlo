import React from 'react'
import { Font } from 'expo'
import { createAppContainer } from 'react-navigation'

import AppNavigator from './navigation/AppNavigator'

const AppContainer = createAppContainer(AppNavigator)

class App extends React.Component {
    state = {
        finishedLoading: false
    }

    async componentDidMount() {
        await Font.loadAsync({
            'open-sans-regular': require('./assets/fonts/OpenSans-Regular.ttf'),
            'fredoka-one-regular': require('./assets/fonts/FredokaOne-Regular.ttf'),
            'oxygen-regular': require('./assets/fonts/Oxygen-Regular.ttf'),
        })

        this.setState({
            finishedLoading: true
        })
    }

    render() {
        const { finishedLoading } = this.state

        return finishedLoading ? <AppContainer /> : null
    }
}

export default App

import { createStackNavigator } from 'react-navigation'

import Home from '../screens/Tasks'
import Edit from '../screens/Tasks/Edit'

export default createStackNavigator({
    TaskHome: Home,
    Edit: Edit
})

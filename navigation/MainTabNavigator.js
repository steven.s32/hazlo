import { createBottomTabNavigator } from 'react-navigation'

import Home from '../screens/Home'
import TasksNavigator from './TasksNavigator'
import Chat from '../screens/Chat'

export default createBottomTabNavigator({
    Tasks: TasksNavigator,
    Home,
    Chat,
}, {
    initialRouteName: 'Home'
})

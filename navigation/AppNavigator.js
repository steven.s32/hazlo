import { createSwitchNavigator } from 'react-navigation'

import TasksNavigator from './TasksNavigator'
import Splash from '../screens/Splash'
import Home from '../screens/Home'
import MainTabNavigator from './MainTabNavigator'

export default createSwitchNavigator({
    Splash: Splash,
    Main: MainTabNavigator,
    Home: Home,
    Tasks: TasksNavigator,
})

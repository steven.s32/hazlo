/* CanonType is meant to be extended, it provides an interface
 * for validation, json encode/decode for data types
 */

class CanonType {
    fromRawObject() {
        throw 'Not implemented!'
    }

    validate() {
        throw 'Not implemented!'
    }

    toJson() {
    }

    fromJson(s) {
        const rawObj = JSON.parse(s)

        try {
            CanonType.fromRawObject(rawObj)
        } catch (e) {
            throw 'JSON payload does not match'
        }
    }
}

const dateIsIso = x => true
const canonDate = x => x

//const ingestGroupData: string => MightContain<GroupData> = expectShape({
const expectMembers = Expect.Map(
    Expect.String,
    { alias: Expect.String }
)

/* we can Expect certain properties to exist in the API response,
 * and Suspect others which are optional
 *
 * Expect failures will throw
 * Suspect failures will be ignored
 */

const ingestGroupData = Expect.Shape({
    members: expectMembers,
    tasks: Expect.Array(Task),
    goals: Suspect.Map(
        Expect.String,
        { title: Suspect.String, description: Expect.String }
    )
})

groupApi.get()
    .then(ingestGroupData)
    .then(groupData => processGroupData)
    .catch(er => dealWithError)

groupApi.put()

const expectObject = ofType => {
    if (ofType instanceof CanonType) {
        return ofType.fromRawObject
    }

    switch (myX.constructor) {
        case ExpectPrimitive:
            return arrayIngester(ofType.fromRawObject)
        default:
            return expectObject(ofType)
    }
}
const expectArray = ofType => arrayIngester(expectObject(ofType))


class Task extends CanonType{
    constructor({title, description, created}) {
        const { by, date } = created

        const canonicalDate = dateIsIso(date) ? date : canonDate(date)

        this.title = title
        this.description = description
        this.created = {
            by,
            date: canonicalDate
        }
    }

    toRawObject() {
    }
    fromRawObject() {
    }
    toJson() { // API boundary
        return JSON.stringify(this.toRawObject())
    }
    fromJson() {
    }
    validate() {
    }
}


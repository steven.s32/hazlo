import React from 'react'
import { NavigationActions } from 'react-navigation'
import { StatusBar, StyleSheet, Text, View, Button } from 'react-native'

class Splash extends React.Component {
    static navigationOptions = {
        header: null,
    }

    render() {
        const { navigate } = this.props.navigation

        return (
            <View style={styles.container}>
                <StatusBar hidden />
                <Text style={styles.title}>hazlo</Text>
                    <Button style={styles.skipButton} title="New Task" onPress={() => navigate('Tasks', {}, NavigationActions.navigate({routeName: 'New'}))} />
                <View style={styles.bottomView}>
                    <Button style={styles.skipButton} title="Skip" onPress={() => navigate('Main')} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#eee',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 75,
        fontFamily: 'fredoka-one-regular',
        textShadowColor: 'rgba(0,0,0,0.1)',
        textShadowOffset: {
            width: 2,
            height: 1
        },
        textShadowRadius: 4,
        padding: 15,
        margin: 15,
        color: '#23c1ff',
    },
    bottomView: {
        position: 'absolute',
        bottom: 20,
    },
    skipButton: {
        fontSize: 80,
    },
})

export default Splash

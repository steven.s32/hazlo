import React from 'react'
import { View, TextInput, Button, Text } from 'react-native'

import { extend } from '../../utility'
import { isVoid } from '../../utility/typeGuards'
import { local, models } from '../../api/persist'

import Center from '../../components/Center'
import ScreenTitle from '../../components/ScreenTitle'
import TwoColumn from '../../components/TwoColumn'

const textWithStyle = style => ({children, ...props}) =>
    <Text style={style} {...props}>{children}</Text>

const BiggerText = textWithStyle({fontSize: 20})
const SmallerText = textWithStyle({fontSize: 15})

const Input = ({style, ...props}) =>
    <TextInput style={{fontSize: 22, paddingVertical: 8, ...style}} {...props} />

export default class TaskEdit extends React.Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)

        const id = props.navigation.getParam('id')

        if (isVoid(id)) {
            this.state = {
                loading: false
            }
        } else {
            this.state = {
                loading: true,
                id
            }
        }
    }

    async componentDidMount() {
        const { id } = this.state

        if (!isVoid(id)) {
            const task = await local.get.one(models.TASK, id)
            console.info('editing', id, task)

            this.setState({ ...task, loading: false })
        }
    }

    updateField = fieldName => value => {
        this.setState({
            [fieldName]: value
        })
    }

    updateTitle = this.updateField('title')
    updateDescription = this.updateField('description')

    submitTask = async () => {
        const { navigation } = this.props
        const { id, loading, ...task} = this.state

        if (loading) {
            return
        }

        if (isVoid(id)) {
            await local.set.new(models.TASK, extend(task, {
                points: 1,
                createdBy: 'me',
                claimedBy: '',
            }))
        } else {
            await local.set.update(models.TASK, id, task)
        }

        navigation.pop()
    }

    render() {
        const { loading, id, title, description, points=1, claimedBy } = this.state

        if (loading) {
            return <Center><Text>Loading task..</Text></Center>
        }

        return (
            <View style={{paddingHorizontal: 20}}>
                <ScreenTitle><Text>Task {id}</Text></ScreenTitle>
                <Input
                    placeholder="Title"
                    onChangeText={this.updateTitle}
                    style={{fontSize: 28}}
                    value={title}
                    multiline
                />
                <Input
                    placeholder="Description"
                    onChangeText={this.updateDescription}
                    value={description}
                    multiline
                />
                <TwoColumn>
                    <BiggerText>Points</BiggerText>
                    <SmallerText>{points}</SmallerText>
                </TwoColumn>
                <Button title="Finish" onPress={this.submitTask} />
            </View>
        )
    }
}


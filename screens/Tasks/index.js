import React from 'react'
import { AsyncStorage, TouchableOpacity, StyleSheet, Text, Button, View, FlatList } from 'react-native'
import { addRandomNumRandomWords, maxRandom, objectToArray } from '../../utility'
import { isVoid } from '../../utility/typeGuards'

import Center from '../../components/Center'
import TwoColumn from '../../components/TwoColumn'
import { local, models } from '../../api/persist'

const buildRandomTask = () => ({
    title: addRandomNumRandomWords(10),
    description: addRandomNumRandomWords(80),
    points: maxRandom(20),
    createdBy: addRandomNumRandomWords(5, 'names'),
    claimedBy: maxRandom(10) < 8 ? addRandomNumRandomWords(5, 'names') : null,
})


const getId = x => String(x.id)

const flatViewer = (view, props) => ({item}) => view({...item, ...props})

const TaskView = ({title, description, points, createdBy, claimedBy, increase, id, editTask}) =>
    <TouchableOpacity onPress={() => editTask(id)}>
        <View style={styles.taskContainer}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.description}>{description}</Text>
            <TwoColumn>
                <Text style={{fontSize: 20}}>Points:</Text>
                <Button title={String(points)} onPress={ () => increase(id) } />
            </TwoColumn>
            <TwoColumn>
                <Text style={{fontSize: 20}}>Created By:</Text>
                <Text style={{fontSize: 15}}>{createdBy}</Text>
            </TwoColumn>
            <TwoColumn>
                <Text style={{fontSize: 20}}>Claimed By:</Text>
                <Text style={{fontSize: 15}}>{claimedBy}</Text>
            </TwoColumn>
        </View>
    </TouchableOpacity>

const styles = StyleSheet.create({
    taskContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    title: {
        fontSize: 28
    },
    description: {
        fontSize: 18
    },
})

const Line = () => <View style={{ borderBottomColor: 'black', borderBottomWidth: StyleSheet.hairlineWidth, height: 3, marginTop: 2, marginBottom: 2 }} />
    
class Tasks extends React.Component {
    state = {
        tasks: {},
        loading: true
    }

    updateTask = update => async id => {
        const { tasks } = this.state
        const task = tasks[id]

        const updatedTask = update(task)

        await local.set.update(models.TASK, task.id, updatedTask)

        this.getTasksFromStorage()
    }

    addTask = async task => {
        await local.set.new(models.TASK, task)

        this.getTasksFromStorage()
    }

    increasePointValue = this.updateTask( task => ({...task, points: task.points + 1 }))

    getTasksFromStorage = async () => {
        const tasks = await local.get.all(models.TASK)

        if (isVoid(tasks)) {
            this.setState({ tasks: [] })
        } else {
            this.setState({ tasks: objectToArray(tasks, 'id') })
        }
    }

    async componentDidMount() {
        this.didFocus = this.props.navigation.addListener(
            'didFocus',
            this.getTasksFromStorage
        )

        await this.getTasksFromStorage()

        this.setState({
            loading: false
        })
    }

    async componentWillUnmount() {
        const { tasks } = this.state

        this.didFocus.remove()

        await Promise.all(
            tasks.map( task => local.set.update(models.TASK, task.id, task) )
        )
    }

    editTask = id => {
        const { navigation } = this.props

        navigation.navigate('Edit', {id})
    }

    render() {
        const { tasks, loading } = this.state

        if (loading) {
            return <Center><Text>Loading..</Text></Center>
        }

        return (
            <Center width="100%">
                <FlatList
                    data={tasks}
                    keyExtractor={getId}
                    renderItem={flatViewer(TaskView, {increase: this.increasePointValue, editTask: this.editTask})}
                    ItemSeparatorComponent={Line}
                    ListHeaderComponent={ () =>
                        <View>
                            <Button title="Clear all app local" onPress={
                                async () => {
                                    await AsyncStorage.clear()
                                    this.getTasksFromStorage()
                                }
                            } />
                            <Button title="Log all keys" onPress={
                                async () => console.info( await AsyncStorage.getAllKeys() )
                            } />
                        </View>
                    }
                    ListFooterComponent={ () =>
                        <Button title="Add random" onPress={
                            () => this.addTask(buildRandomTask())
                        } />
                    }
                />
            </Center>
        )
    }
}

Tasks.navigationOptions = {
    header: null
}

export default Tasks

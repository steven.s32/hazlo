import React from 'react'
import { StyleSheet, StatusBar } from 'react-native'

import Center from '../components/Center'
import ScreenTitle from '../components/ScreenTitle'
import Paragraph from '../components/Paragraph'
import ScreenWrapper from '../components/ScreenWrapper'

const Home = () =>
    <ScreenWrapper>
        <StatusBar
            barStyle="dark-content"
            backgroundColor="#FFFFFF"
        />
        <ScreenTitle>Home</ScreenTitle>
        <Paragraph>Looks like you don't have any tasks due</Paragraph>
        <Center>
            <Paragraph>You're not waiting on any tasks to get done</Paragraph>
        </Center>
        <Center>
            <Paragraph>Going alright</Paragraph>
        </Center>
    </ScreenWrapper>

Home.navigationOptions = {
    header: null,
}

export default Home

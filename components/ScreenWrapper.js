import React from 'react'
import { View } from 'react-native'

const ScreenWrapper = ({children}) =>
    <View style={{display: 'flex', paddingHorizontal: 18, paddingTop: 22, minHeight: '100%'}}>{children}</View>

export default ScreenWrapper

import React from 'react'
import { StyleSheet, View } from 'react-native'

const styles = StyleSheet.create({
    centered: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default ({children}) => <View style={styles.centered}>{children}</View>

import React from 'react'
import { StyleSheet, Text } from 'react-native'

const ScreenTitle = ({children}) =>
    <Text style={style.screenTitle}>{children}</Text>

const style = StyleSheet.create({
    screenTitle: {
        fontSize: 34,
        fontFamily: 'open-sans-regular',
    }
})

export default ScreenTitle

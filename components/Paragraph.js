import React from 'react'
import { StyleSheet, Text } from 'react-native'

const Paragraph = ({children}) =>
    <Text style={style.paragraph}>{children}</Text>

const style = StyleSheet.create({
    paragraph: {
        fontSize: 16,
        fontFamily: 'open-sans-regular',
    }
})

export default Paragraph


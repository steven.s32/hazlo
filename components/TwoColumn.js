import React from 'react'
import { View } from 'react-native'

const TwoColumn = ({children, style, onPress}) =>
    <View onPress={onPress} style={{justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', ...style}}>
        {children}
    </View>

export default TwoColumn
